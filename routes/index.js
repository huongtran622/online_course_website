var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/contact', function(req, res, next) {
  res.render('contact');
});

router.get('/courses', function(req, res, next) {
  res.render('courses');
});

router.get('/blogs', function(req, res, next) {
  res.render('blogs');
});

router.get('/aboutus', function(req, res, next) {
  res.render('aboutus');
});

router.get('/register', function(req, res, next) {
  res.render('register');
});

module.exports = router;
